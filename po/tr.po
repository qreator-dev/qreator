# Turkish translation for qreator
# Copyright (c) 2012 Rosetta Contributors and Canonical Ltd 2012
# This file is distributed under the same license as the qreator package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: qreator\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2013-05-19 10:42+0200\n"
"PO-Revision-Date: 2013-11-24 22:18+0000\n"
"Last-Translator: Mustafa Yılmaz <apshalasha@gmail.com>\n"
"Language-Team: Turkish <tr@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2016-06-21 05:53+0000\n"
"X-Generator: Launchpad (build 18097)\n"

#: ../data/ui/QrCodeLocation.ui.h:1
msgid "Latitude:"
msgstr "Enlem:"

#: ../data/ui/QrCodeLocation.ui.h:2
msgid "Longitude:"
msgstr "Boylam:"

#: ../data/ui/QrCodeLocation.ui.h:3
msgid "Location details"
msgstr "Konum ayrıntıları"

#: ../qreator/qrcodes/QRCodeSoftwareCenterAppGtk.py:128
msgid "[Wait a few seconds to enable autocompletion...]"
msgstr "[Otomatik tamamlamanın açılması için birkaç saniye bekleyin...]"

#: ../qreator/qrcodes/QRCodeSoftwareCenterAppGtk.py:146
msgid "[Type the name of an app]"
msgstr "[Bir uygulama adı yazın]"

#. TRANSLATORS: this refers to the is.gd URL shortening service
#: ../qreator/qrcodes/QRCodeURLGtk.py:27
msgid "Isgd"
msgstr "Isgd"

#. TRANSLATORS: this refers to the tinyurl.com URL shortening service
#: ../qreator/qrcodes/QRCodeURLGtk.py:43
msgid "TinyUrl"
msgstr "TinyUrl"

#. TRANSLATORS: this refers to the bit.ly URL shortening service
#: ../qreator/qrcodes/QRCodeURLGtk.py:56
msgid "Bitly"
msgstr "Bitly"

#. TRANSLATORS: this refers to the goo.gl URL shortening service
#: ../qreator/qrcodes/QRCodeURLGtk.py:73
msgid "Google"
msgstr "Google"

#. Initialize placeholder text (we need to do that because due to
#. a Glade bug they are otherwise not marked as translatable)
#: ../qreator/qrcodes/QRCodeURLGtk.py:115
msgid "[URL]"
msgstr "[URL]"

#: ../qreator/qrcodes/QRCodeURLGtk.py:179
msgid "A network connection error occured: {0}"
msgstr "Bir ağ bağlantı hatası gerçekleşti: {0}"

#: ../qreator/qrcodes/QRCodeURLGtk.py:187
msgid "An error occured while trying to shorten the URL: {0}"
msgstr "URL kısaltılmaya çalışılırken bir hata gerçekleşti: {0}"

#: ../qreator/qrcodes/QRCodeText.py:22 ../qreator/qrcodes/QRCodeVCardGtk.py:67
msgid "Text"
msgstr "Metin"

#: ../qreator/qrcodes/QRCodeText.py:26
msgid "Create a QR code from text"
msgstr "Metinden bir kare kod oluştur"

#: ../qreator/qrcodes/QRCodeLocation.py:22
msgid "Geolocation"
msgstr "Coğrafî Konum"

#: ../qreator/qrcodes/QRCodeLocation.py:26
msgid "Create a QR code for a location"
msgstr "Bir konum bilgisinden Kare kod oluştur"

#. TRANSLATORS: this refers to a phone call QR code type
#: ../qreator/qrcodes/QRCodeSMSGtk.py:25
msgid "Call"
msgstr "Çağrı"

#. TRANSLATORS: this refers to an SMS message QR code type
#: ../qreator/qrcodes/QRCodeSMSGtk.py:27
msgid "Text Message"
msgstr "Metin Mesajı"

#: ../qreator/qrcodes/QRCodeSMSGtk.py:56
msgid ""
"  [Text message. Some code readers might not support this QR code type]"
msgstr ""
"  [Metin mesajı. Bazı kod okuyucular bu QR kod biçimini desteklemeyebilir]"

#: ../qreator/qrcodes/QRCodeWifi.py:22
msgid "Wifi network"
msgstr "Kablosuz Ağ"

#: ../qreator/qrcodes/QRCodeWifi.py:26
msgid "Create a QR code for WiFi settings"
msgstr "Kablosuz ayarları için bir QR kod oluştur"

#: ../qreator/qrcodes/QRCodeWifiGtk.py:63
msgid "[Network identifier - expand for autodetection]"
msgstr "[Ağ tanımlayıcısı - otomatik tanıma için genişlet]"

#: ../qreator/qrcodes/QRCodeWifiGtk.py:64
msgid "[Network password]"
msgstr "[Ağ şifresi]"

#: ../qreator.desktop.in.h:1 ../data/ui/QreatorWindow.ui.h:3
msgid "Qreator"
msgstr "Qreator"

#: ../qreator.desktop.in.h:2 ../data/ui/QreatorWindow.ui.h:19
msgid "Create your own QR codes"
msgstr "Kendi kare kodunuzu oluşturun"

#: ../qreator.desktop.in.h:3
msgid "New QR code for URL"
msgstr "URL için yeni kare kod"

#: ../qreator.desktop.in.h:4
msgid "New QR code for Text"
msgstr "Metin için yeni kare kod"

#: ../qreator.desktop.in.h:5
msgid "New QR code for Location"
msgstr "Konum bilgisi için yeni kare kod"

#: ../qreator.desktop.in.h:6
msgid "New QR code for WiFi network"
msgstr "Yazılım merkezi uygulaması için yeni kare kod"

#: ../qreator.desktop.in.h:7
msgid "New QR code for a Software Center app"
msgstr "Yazılım merkezi uygulaması için yeni kare kod"

#: ../qreator.desktop.in.h:8
msgid "New QR code for a Business card"
msgstr "Kartvizit için bir kare kod"

#: ../qreator.desktop.in.h:9
msgid "New QR code for a Call or text message"
msgstr "Çağrı veya metin mesajı için yeni kare kod"

#: ../qreator/qrcodes/QRCodeURL.py:22
msgid "URL"
msgstr "Yerimi"

#: ../qreator/qrcodes/QRCodeURL.py:26
msgid "Create a QR code for a URL"
msgstr "URL için kare kod oluştur"

#: ../data/ui/QrCodeSoftwareCentreApp.ui.h:1
msgid "Ubuntu Software Center app:"
msgstr "Ubuntu Yazılım Merkezi uygulaması:"

#. TRANSLATORS: this refers to the button to shorten a URL for the URL QR code type
#: ../data/ui/QrCodeURL.ui.h:2
msgid "Shorten"
msgstr "Kısaltılmış"

#: ../data/ui/QrCodeSMS.ui.h:1
msgid "Phone:"
msgstr "Telefon:"

#: ../data/ui/QrCodeSMS.ui.h:2
msgid "Type: "
msgstr "Tür: "

#: ../data/ui/QrCodeSMS.ui.h:3
msgid "Message: "
msgstr "Mesaj: "

#: ../qreator/qrcodes/QRCodeSoftwareCenterApp.py:23
msgid "Ubuntu Software Center app"
msgstr "Ubuntu Yazılım merkezi uygulaması"

#: ../qreator/qrcodes/QRCodeSoftwareCenterApp.py:27
msgid "Create a QR code for an app from the Software Center"
msgstr "Yazılım Merkezinden bir uygulama için bir kare kod oluştur"

#: ../qreator/qrcodes/QRCodeVCard.py:23
msgid "Business card"
msgstr "Kartvizit"

#: ../qreator/qrcodes/QRCodeVCard.py:27
msgid "Create a QR code for a business card"
msgstr "Kartvizit için bir kare kod oluştur"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:43
msgid "E-mail"
msgstr "E-posta"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:44
msgid "Phone"
msgstr "Telefon"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:45
msgid "Address"
msgstr "Adres"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:46
msgid "Website"
msgstr "Web sitesi"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:47
msgid "Nickname"
msgstr "Takma ad"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:48
msgid "Title"
msgstr "Unvan"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:49
msgid "Role"
msgstr "Görev"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:50
msgid "Note"
msgstr "Not"

#. TRANSLATORS:
#. text: Indicates that the telephone number supports text messages (SMS).
#. voice: Indicates a voice telephone number.
#. fax: Indicates a facsimile telephone number.
#. cell: Indicates a cellular or mobile telephone number.
#. video: Indicates a video conferencing telephone number.
#. pager: Indicates a paging device telephone number.
#. textphone: Indicates a telecommunication device for people with
#. hearing or speech difficulties.
#: ../qreator/qrcodes/QRCodeVCardGtk.py:63
msgid "Cell"
msgstr "Cep"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:64
msgid "Voice"
msgstr "Ses"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:65
msgid "Fax"
msgstr "Faks"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:66
msgid "Video"
msgstr "Görüntü"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:68
msgid "Pager"
msgstr "Çağrı Cihazı"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:69
msgid "Textphone"
msgstr "Metin telefonu"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:72
msgid "Home"
msgstr "Ev"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:73
msgid "Work"
msgstr "İş"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:121
msgid "Remove this field"
msgstr "Bu alanı kaldır"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:289
msgid "Street"
msgstr "Cadde"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:290
msgid "City"
msgstr "İl"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:291
msgid "Region"
msgstr "Bölge"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:292
msgid "Zip code"
msgstr "Posta Kodu"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:293
msgid "Country"
msgstr "Ülke"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:294
msgid "Postbox"
msgstr "Posta kutusu"

#. TRANSLATORS: This refers to the most likely family name {0} -
#. given name {1} order in the language
#: ../qreator/qrcodes/QRCodeVCardGtk.py:354
msgid "{1} {0}"
msgstr "{1} {0}"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:356
#: ../qreator/qrcodes/QRCodeVCardGtk.py:358
msgid "[Required for a valid business card]"
msgstr "[Geçerli bir kartvizit için gerekli]"

#: ../data/ui/QrCodeVCard.ui.h:1
msgid "Family name:"
msgstr "Soyad:"

#: ../data/ui/QrCodeVCard.ui.h:2
msgid "Given name:"
msgstr "Verilen ad:"

#: ../data/ui/QrCodeVCard.ui.h:3
msgid "Full name:"
msgstr "Tam ad:"

#: ../data/ui/QrCodeVCard.ui.h:4
msgid "Swap full name components"
msgstr "Ad ve soyadı yer değiştir"

#: ../data/ui/QrCodeVCard.ui.h:5
msgid "Add an additional field"
msgstr "İlave alan ekle"

#: ../data/ui/QreatorWindow.ui.h:1
msgid "Change foreground color"
msgstr "Önalan rengini değiştir"

#: ../data/ui/QreatorWindow.ui.h:2
msgid "Change background color"
msgstr "Arkaplan rengini değiştir"

#: ../data/ui/QreatorWindow.ui.h:4
msgid "_File"
msgstr "_Dosya"

#: ../data/ui/QreatorWindow.ui.h:5
msgid "_Edit"
msgstr "_Düzenle"

#: ../data/ui/QreatorWindow.ui.h:6
msgid "Create a new QR code"
msgstr "Yeni Kare Kod Oluştur"

#: ../data/ui/QreatorWindow.ui.h:7
msgid "New"
msgstr "Yeni"

#: ../data/ui/QreatorWindow.ui.h:8
msgid "Browse QR codes history"
msgstr "Kare kod geçmişini görüntüle"

#: ../data/ui/QreatorWindow.ui.h:9
msgid "History"
msgstr "Geçmiş"

#: ../data/ui/QreatorWindow.ui.h:10
msgid "About this application"
msgstr "Bu uygulama hakkında"

#: ../data/ui/QreatorWindow.ui.h:11
msgid "About"
msgstr "Hakkında"

#: ../data/ui/QreatorWindow.ui.h:12
msgid "Not in your language yet?"
msgstr "Sizin dilinizde değil mi?"

#: ../data/ui/QreatorWindow.ui.h:13
msgid "Something not working?"
msgstr "Bir şey mi çalışmıyor?"

#: ../data/ui/QreatorWindow.ui.h:14
msgid "Do you want to improve it?"
msgstr "Geliştirmek mi istiyorsunuz?"

#: ../data/ui/QreatorWindow.ui.h:15
msgid "Translate it!"
msgstr "Tercüme et!"

#: ../data/ui/QreatorWindow.ui.h:16
msgid "Report a bug!"
msgstr "Hata bildirimi yap!"

#: ../data/ui/QreatorWindow.ui.h:17
msgid "Contribute code!"
msgstr "Koduna katkıda bulun!"

#: ../data/ui/QreatorWindow.ui.h:18
msgid ""
"If you use Qreator and you like it, please consider giving a hand in making "
"it better for you and many others. Thank you!"
msgstr ""
"Qreator'u kullanıyor ve seviyorsanız, onu hem siz hem de başkaları için daha "
"iyi yapmak için yardım etmeyi lütfen düşünün. Teşekkürler!"

#: ../data/ui/QreatorWindow.ui.h:20
msgid "Save"
msgstr "Kaydet"

#: ../data/ui/QreatorWindow.ui.h:21
msgid "Save QR code to disk"
msgstr "Kare kodu belleğe kaydet"

#: ../data/ui/QreatorWindow.ui.h:22
msgid "Copy"
msgstr "Kopyala"

#: ../data/ui/QreatorWindow.ui.h:23
msgid "Copy QR code to clipboard"
msgstr "Kare kodu panoya kopyala"

#: ../data/ui/QreatorWindow.ui.h:24
msgid "Print"
msgstr "Yazdır"

#: ../data/ui/QreatorWindow.ui.h:25
msgid "Print QR code"
msgstr "Kare kodu yazdır"

#: ../data/ui/QreatorWindow.ui.h:26
msgid "Edit"
msgstr "Düzenle"

#: ../data/ui/QreatorWindow.ui.h:27
msgid "Edit QR code"
msgstr "Kare kodu düzenle"

#: ../data/ui/QreatorWindow.ui.h:28
msgid "Change the QR code colors"
msgstr "Kare kodun renklerini değiştir"

#: ../data/ui/QreatorWindow.ui.h:29
msgid "Swap the foreground and background color"
msgstr "Ön ve arkaplan renklerini yer değiştir"

#: ../data/ui/QreatorWindow.ui.h:30
msgid "Reset to defaults"
msgstr "Varsayılanlara dön"

#: ../qreator/qrcodes/QRCodeSMS.py:23
msgid "Phone call and messaging"
msgstr "Telefon görüşmesi ve mesajlaşma"

#: ../qreator/qrcodes/QRCodeSMS.py:27
msgid "Create a QR code to initiate calls or send text messages"
msgstr "Aramayı veya metin mesajları yollamayı tetikleyen kare kod oluştur"

#: ../data/ui/QrCodeWifi.ui.h:1
msgid "Security:"
msgstr "Güvenlik:"

#: ../data/ui/QrCodeWifi.ui.h:2
msgid "SSID:"
msgstr "SSID:"

#: ../data/ui/QrCodeWifi.ui.h:3
msgid "Password:"
msgstr "Açarsöz:"

#: ../data/ui/QrCodeWifi.ui.h:4
msgid "Show Password"
msgstr "Açarsözü Göster"

#: ../qreator/QreatorWindow.py:197
msgid "Distributed under the GPL v3 license."
msgstr "GPL v3 lisansı altında dağıtılmıştır."

#. Gtk.AboutDialog does not recognize https and would not render the
#. markup otherwise
#: ../qreator/QreatorWindow.py:207
msgid "translator-credits"
msgstr ""
"Launchpad Contributions:\n"
"  David Planella https://launchpad.net/~dpm\n"
"  Mustafa Yılmaz https://launchpad.net/~apshalasha\n"
"  Volkan Gezer https://launchpad.net/~volkangezer\n"
"  ubunturk https://launchpad.net/~vozdamar\n"
"  Şâkir Aşçı https://launchpad.net/~sakirasci"

#: ../qreator/QreatorWindow.py:320
msgid "Please choose a file"
msgstr "Lütfen bir dosya seçin"

#: ../qreator/QreatorWindow.py:326
msgid "PNG images"
msgstr "PNG görüntüleri"

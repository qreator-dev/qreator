# Bengali translation for qreator
# Copyright (c) 2012 Rosetta Contributors and Canonical Ltd 2012
# This file is distributed under the same license as the qreator package.
# Mahay Alam Khan <mak@ankur.org.bd>, 2012.
# Newton Baidya <newton@ankur.org.bd>, 2012.
# Dewan Jerin Sultana <jerin@ankur.org.bd>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: qreator\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2013-05-19 10:42+0200\n"
"PO-Revision-Date: 2012-11-11 11:54+0000\n"
"Last-Translator: Kazi Shahnoor Ashraf <kaziweb1@gmail.com>\n"
"Language-Team: Bengali <bn@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2016-06-21 05:53+0000\n"
"X-Generator: Launchpad (build 18097)\n"

#: ../data/ui/QrCodeLocation.ui.h:1
msgid "Latitude:"
msgstr "অক্ষাংশ:"

#: ../data/ui/QrCodeLocation.ui.h:2
msgid "Longitude:"
msgstr "দ্রাঘিমাংশ:"

#: ../data/ui/QrCodeLocation.ui.h:3
msgid "Location details"
msgstr "অবস্থানের বিস্তারিত তথ্য"

#: ../qreator/qrcodes/QRCodeSoftwareCenterAppGtk.py:128
msgid "[Wait a few seconds to enable autocompletion...]"
msgstr ""
"[স্বয়ংক্রিয় সম্পূর্ণকরণ সক্রিয় করার জন্য কয়েক সেকেন্ড অপেক্ষা করুন...]"

#: ../qreator/qrcodes/QRCodeSoftwareCenterAppGtk.py:146
msgid "[Type the name of an app]"
msgstr "[একটি অ্যাপ্লিকেশনের নাম লিখুন]"

#. TRANSLATORS: this refers to the is.gd URL shortening service
#: ../qreator/qrcodes/QRCodeURLGtk.py:27
msgid "Isgd"
msgstr ""

#. TRANSLATORS: this refers to the tinyurl.com URL shortening service
#: ../qreator/qrcodes/QRCodeURLGtk.py:43
msgid "TinyUrl"
msgstr ""

#. TRANSLATORS: this refers to the bit.ly URL shortening service
#: ../qreator/qrcodes/QRCodeURLGtk.py:56
msgid "Bitly"
msgstr ""

#. TRANSLATORS: this refers to the goo.gl URL shortening service
#: ../qreator/qrcodes/QRCodeURLGtk.py:73
msgid "Google"
msgstr ""

#. Initialize placeholder text (we need to do that because due to
#. a Glade bug they are otherwise not marked as translatable)
#: ../qreator/qrcodes/QRCodeURLGtk.py:115
msgid "[URL]"
msgstr "[URL]"

#: ../qreator/qrcodes/QRCodeURLGtk.py:179
msgid "A network connection error occured: {0}"
msgstr ""

#: ../qreator/qrcodes/QRCodeURLGtk.py:187
msgid "An error occured while trying to shorten the URL: {0}"
msgstr ""

#: ../qreator/qrcodes/QRCodeText.py:22 ../qreator/qrcodes/QRCodeVCardGtk.py:67
msgid "Text"
msgstr "টেক্সট"

#: ../qreator/qrcodes/QRCodeText.py:26
msgid "Create a QR code from text"
msgstr ""

#: ../qreator/qrcodes/QRCodeLocation.py:22
msgid "Geolocation"
msgstr "ভৌগলিক অবস্থান"

#: ../qreator/qrcodes/QRCodeLocation.py:26
msgid "Create a QR code for a location"
msgstr ""

#. TRANSLATORS: this refers to a phone call QR code type
#: ../qreator/qrcodes/QRCodeSMSGtk.py:25
msgid "Call"
msgstr ""

#. TRANSLATORS: this refers to an SMS message QR code type
#: ../qreator/qrcodes/QRCodeSMSGtk.py:27
msgid "Text Message"
msgstr ""

#: ../qreator/qrcodes/QRCodeSMSGtk.py:56
msgid ""
"  [Text message. Some code readers might not support this QR code type]"
msgstr ""

#: ../qreator/qrcodes/QRCodeWifi.py:22
msgid "Wifi network"
msgstr "ওয়াইফাই নেটওয়ার্ক"

#: ../qreator/qrcodes/QRCodeWifi.py:26
msgid "Create a QR code for WiFi settings"
msgstr ""

#: ../qreator/qrcodes/QRCodeWifiGtk.py:63
msgid "[Network identifier - expand for autodetection]"
msgstr "[নেটওয়ার্ক শনাক্তকারী - স্বয়ংক্রিয় সনাক্তকরণের জন্য প্রসারিত করুন]"

#: ../qreator/qrcodes/QRCodeWifiGtk.py:64
msgid "[Network password]"
msgstr "[নেটওয়ার্ক পাসওয়ার্ড]"

#: ../qreator.desktop.in.h:1 ../data/ui/QreatorWindow.ui.h:3
msgid "Qreator"
msgstr "কিউরেটর"

#: ../qreator.desktop.in.h:2 ../data/ui/QreatorWindow.ui.h:19
msgid "Create your own QR codes"
msgstr "আপনার নিজের QR কোড তৈরি করুন"

#: ../qreator.desktop.in.h:3
msgid "New QR code for URL"
msgstr "URL এর জন্যে নতুন QR কোড"

#: ../qreator.desktop.in.h:4
msgid "New QR code for Text"
msgstr "টেক্সটের জন্য নতুন QR কোড"

#: ../qreator.desktop.in.h:5
msgid "New QR code for Location"
msgstr "স্থানের জন্যে নতুন QR কোড"

#: ../qreator.desktop.in.h:6
msgid "New QR code for WiFi network"
msgstr "ওয়াইফাই নেটওয়ার্ক এর জন্যে নতুন QR কোড"

#: ../qreator.desktop.in.h:7
msgid "New QR code for a Software Center app"
msgstr ""

#: ../qreator.desktop.in.h:8
msgid "New QR code for a Business card"
msgstr ""

#: ../qreator.desktop.in.h:9
msgid "New QR code for a Call or text message"
msgstr ""

#: ../qreator/qrcodes/QRCodeURL.py:22
msgid "URL"
msgstr "URL"

#: ../qreator/qrcodes/QRCodeURL.py:26
msgid "Create a QR code for a URL"
msgstr ""

#: ../data/ui/QrCodeSoftwareCentreApp.ui.h:1
msgid "Ubuntu Software Center app:"
msgstr "উবুন্টু সফটওয়্যার সেন্টার অ্যাপ্লিকেশন:"

#. TRANSLATORS: this refers to the button to shorten a URL for the URL QR code type
#: ../data/ui/QrCodeURL.ui.h:2
msgid "Shorten"
msgstr ""

#: ../data/ui/QrCodeSMS.ui.h:1
msgid "Phone:"
msgstr ""

#: ../data/ui/QrCodeSMS.ui.h:2
msgid "Type: "
msgstr ""

#: ../data/ui/QrCodeSMS.ui.h:3
msgid "Message: "
msgstr ""

#: ../qreator/qrcodes/QRCodeSoftwareCenterApp.py:23
msgid "Ubuntu Software Center app"
msgstr "উবুন্টু সফটওয়্যার সেন্টার অ্যাপ্লিকেশন"

#: ../qreator/qrcodes/QRCodeSoftwareCenterApp.py:27
msgid "Create a QR code for an app from the Software Center"
msgstr ""

#: ../qreator/qrcodes/QRCodeVCard.py:23
msgid "Business card"
msgstr "ব্যবসায়িক কার্ড"

#: ../qreator/qrcodes/QRCodeVCard.py:27
msgid "Create a QR code for a business card"
msgstr ""

#: ../qreator/qrcodes/QRCodeVCardGtk.py:43
msgid "E-mail"
msgstr "ই মেইল"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:44
msgid "Phone"
msgstr "ফোন"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:45
msgid "Address"
msgstr "ঠিকানা"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:46
msgid "Website"
msgstr "ওয়েবসাইট"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:47
msgid "Nickname"
msgstr "ডাকনাম"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:48
msgid "Title"
msgstr "শিরোনাম"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:49
msgid "Role"
msgstr "ভূমিকা"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:50
msgid "Note"
msgstr "নোট"

#. TRANSLATORS:
#. text: Indicates that the telephone number supports text messages (SMS).
#. voice: Indicates a voice telephone number.
#. fax: Indicates a facsimile telephone number.
#. cell: Indicates a cellular or mobile telephone number.
#. video: Indicates a video conferencing telephone number.
#. pager: Indicates a paging device telephone number.
#. textphone: Indicates a telecommunication device for people with
#. hearing or speech difficulties.
#: ../qreator/qrcodes/QRCodeVCardGtk.py:63
msgid "Cell"
msgstr "সেল"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:64
msgid "Voice"
msgstr "কন্ঠ"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:65
msgid "Fax"
msgstr "ফ্যাক্স"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:66
msgid "Video"
msgstr "ভিডিও"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:68
msgid "Pager"
msgstr "পেজার"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:69
msgid "Textphone"
msgstr "টেক্সটফোন"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:72
msgid "Home"
msgstr "হোম"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:73
msgid "Work"
msgstr "কাজ"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:121
msgid "Remove this field"
msgstr "এই ক্ষেত্র অপসারণ করুন"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:289
msgid "Street"
msgstr "রাস্তা"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:290
msgid "City"
msgstr "শহর"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:291
msgid "Region"
msgstr "এলাকা"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:292
msgid "Zip code"
msgstr "জিপ কোড"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:293
msgid "Country"
msgstr "দেশ"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:294
msgid "Postbox"
msgstr "পোস্টবক্স"

#. TRANSLATORS: This refers to the most likely family name {0} -
#. given name {1} order in the language
#: ../qreator/qrcodes/QRCodeVCardGtk.py:354
msgid "{1} {0}"
msgstr "{1} {0}"

#: ../qreator/qrcodes/QRCodeVCardGtk.py:356
#: ../qreator/qrcodes/QRCodeVCardGtk.py:358
msgid "[Required for a valid business card]"
msgstr "[একটি বৈধ ব্যবসার কার্ডের জন্য আবশ্যক]"

#: ../data/ui/QrCodeVCard.ui.h:1
msgid "Family name:"
msgstr "পরিবারিক নাম:"

#: ../data/ui/QrCodeVCard.ui.h:2
msgid "Given name:"
msgstr "প্রদত্ত নাম:"

#: ../data/ui/QrCodeVCard.ui.h:3
msgid "Full name:"
msgstr "পূর্ণ নাম:"

#: ../data/ui/QrCodeVCard.ui.h:4
msgid "Swap full name components"
msgstr "পূর্ণনামের উপাদানসমুহ বিনিময় করুন"

#: ../data/ui/QrCodeVCard.ui.h:5
msgid "Add an additional field"
msgstr "অতিরিক্ত ক্ষেত্র সংযুক্ত করুন"

#: ../data/ui/QreatorWindow.ui.h:1
msgid "Change foreground color"
msgstr ""

#: ../data/ui/QreatorWindow.ui.h:2
msgid "Change background color"
msgstr ""

#: ../data/ui/QreatorWindow.ui.h:4
msgid "_File"
msgstr "ফাইল (_F)"

#: ../data/ui/QreatorWindow.ui.h:5
msgid "_Edit"
msgstr "সম্পাদনা (_E)"

#: ../data/ui/QreatorWindow.ui.h:6
msgid "Create a new QR code"
msgstr "একটি নতুন QR কোড তৈরি করুন"

#: ../data/ui/QreatorWindow.ui.h:7
msgid "New"
msgstr "নতুন"

#: ../data/ui/QreatorWindow.ui.h:8
msgid "Browse QR codes history"
msgstr "QR কোড ইতিহাস ব্রাউজ করুন"

#: ../data/ui/QreatorWindow.ui.h:9
msgid "History"
msgstr "ইতিহাস"

#: ../data/ui/QreatorWindow.ui.h:10
msgid "About this application"
msgstr "এই অ্যাপ্লিকেশনের পরিচিতি"

#: ../data/ui/QreatorWindow.ui.h:11
msgid "About"
msgstr "পরিচিতি"

#: ../data/ui/QreatorWindow.ui.h:12
msgid "Not in your language yet?"
msgstr "এখনো আপনার ভাষায় নেই?"

#: ../data/ui/QreatorWindow.ui.h:13
msgid "Something not working?"
msgstr "কোন কিছু কাজ করছে না?"

#: ../data/ui/QreatorWindow.ui.h:14
msgid "Do you want to improve it?"
msgstr "আপনি কি এটা উন্নত করতে চান?"

#: ../data/ui/QreatorWindow.ui.h:15
msgid "Translate it!"
msgstr "অনুবাদ করুন!"

#: ../data/ui/QreatorWindow.ui.h:16
msgid "Report a bug!"
msgstr "একটি বাগ রিপোর্ট করুন!"

#: ../data/ui/QreatorWindow.ui.h:17
msgid "Contribute code!"
msgstr "কোড প্রদান করুন!"

#: ../data/ui/QreatorWindow.ui.h:18
msgid ""
"If you use Qreator and you like it, please consider giving a hand in making "
"it better for you and many others. Thank you!"
msgstr ""
"আপনি যদি কিউরেটর ব্যবহার করে থাকেন এবং এটাকে পছন্দ করে থাকেন, তাহলে অনুগ্রহ "
"করে আপনার এবং আরো অনেকের জন্য এটাকে উন্নত করতে সহযোগিতা করুন।ধন্যবাদ!"

#: ../data/ui/QreatorWindow.ui.h:20
msgid "Save"
msgstr "সংরক্ষণ করুন"

#: ../data/ui/QreatorWindow.ui.h:21
msgid "Save QR code to disk"
msgstr "ডিস্কের মধ্যে QR কোড সংরক্ষণ করুন"

#: ../data/ui/QreatorWindow.ui.h:22
msgid "Copy"
msgstr "অনুলিপি করুন"

#: ../data/ui/QreatorWindow.ui.h:23
msgid "Copy QR code to clipboard"
msgstr "ক্লিপবোর্ডে QR কোড অনুলিপি করুন"

#: ../data/ui/QreatorWindow.ui.h:24
msgid "Print"
msgstr ""

#: ../data/ui/QreatorWindow.ui.h:25
msgid "Print QR code"
msgstr ""

#: ../data/ui/QreatorWindow.ui.h:26
msgid "Edit"
msgstr "সম্পাদনা করুন"

#: ../data/ui/QreatorWindow.ui.h:27
msgid "Edit QR code"
msgstr ""

#: ../data/ui/QreatorWindow.ui.h:28
msgid "Change the QR code colors"
msgstr ""

#: ../data/ui/QreatorWindow.ui.h:29
msgid "Swap the foreground and background color"
msgstr ""

#: ../data/ui/QreatorWindow.ui.h:30
msgid "Reset to defaults"
msgstr ""

#: ../qreator/qrcodes/QRCodeSMS.py:23
msgid "Phone call and messaging"
msgstr ""

#: ../qreator/qrcodes/QRCodeSMS.py:27
msgid "Create a QR code to initiate calls or send text messages"
msgstr ""

#: ../data/ui/QrCodeWifi.ui.h:1
msgid "Security:"
msgstr "নিরাপত্তা:"

#: ../data/ui/QrCodeWifi.ui.h:2
msgid "SSID:"
msgstr "SSID:"

#: ../data/ui/QrCodeWifi.ui.h:3
msgid "Password:"
msgstr "পাসওয়ার্ড:"

#: ../data/ui/QrCodeWifi.ui.h:4
msgid "Show Password"
msgstr "পাসওয়ার্ড প্রদর্শন করুন"

#: ../qreator/QreatorWindow.py:197
msgid "Distributed under the GPL v3 license."
msgstr "GPL v3 লাইসেন্সের অধীনে বন্টিত।"

#. Gtk.AboutDialog does not recognize https and would not render the
#. markup otherwise
#: ../qreator/QreatorWindow.py:207
msgid "translator-credits"
msgstr ""
"Launchpad Contributions:\n"
"  Kazi Shahnoor Ashraf https://launchpad.net/~kaziweb\n"
"  Mahay Alam Khan https://launchpad.net/~mahayalamkhan"

#: ../qreator/QreatorWindow.py:320
msgid "Please choose a file"
msgstr "অনুগ্রহপূর্বক একটি ফাইল পছন্দ করুন"

#: ../qreator/QreatorWindow.py:326
msgid "PNG images"
msgstr "PNG চিত্র"
